import { Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { Public } from './security/auth/public/public.decorator';
import { AuthService } from './security/auth/auth.service';
import { LocalAuthGuard } from './security/auth/guards/local-auth.guard';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { User } from './data-model/user/entities/user.entity';

@ApiTags('auth')
@Controller()
export class AppController {
  constructor(private authService: AuthService) {}

  @Public()
  @ApiOperation({ summary: 'get jwt for user' })
  @ApiResponse({ status: 201, description: 'Successful authorization' })
  @UseGuards(LocalAuthGuard)
  @Post('/auth/login')
  async login(@Req() req : {user: {username: string, password: string}}) {
    return this.authService.login(req.user);
  }

  @Get('profile')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'get authorizated user data (whisuot id and password)' })
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: User,
  })
  getProfile(@Req() req) {
    return req.user;
  }
}
