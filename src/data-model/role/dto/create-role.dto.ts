import { ApiProperty } from "@nestjs/swagger";
import { Prisma } from "@prisma/client";
import { IsNotEmpty } from "class-validator";

export class CreateRoleDto implements Prisma.RolesCreateInput {
    @ApiProperty({ description: "Role name", nullable: false })
    @IsNotEmpty({ message: 'Это обязательное поле' })
    name: string;
    users?: Prisma.UserCreateNestedManyWithoutRoleInput;
}
