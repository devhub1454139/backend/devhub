import { PartialType } from '@nestjs/mapped-types';
import { CreateRoleDto } from './create-role.dto';
import { Prisma } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateRoleDto extends PartialType(CreateRoleDto) implements Prisma.RolesUpdateInput {
    @ApiProperty({ description: "Role name", nullable: true })
    name?: string;
}
