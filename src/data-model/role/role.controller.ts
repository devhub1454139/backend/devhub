import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { RoleService } from './role.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Role } from './entities/role.entity';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('data-model')
@Controller('role')
export class RoleController {
  constructor(private readonly roleService: RoleService) {}

  @Post()
  @ApiBearerAuth()
  @ApiOperation({ summary: 'create a new role' })
  @ApiResponse({ status: 201, description: 'Successful create' })
  async create(@Body() createRoleDto: CreateRoleDto): Promise<Role> {
    return this.roleService.create(createRoleDto);
  }

  @Get()
  @ApiBearerAuth()
  @ApiOperation({ summary: 'get json of all roles' })
  @ApiResponse({ status: 200, description: 'Success' })
  findAll() {
    return this.roleService.findMany({});
  }

  @Get(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'get json of one role' })
  @ApiResponse({ status: 200, description: 'Success' })
  findOne(@Param('id') id: number) {
    return this.roleService.findOne({ id });
  }

  @Patch(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'update role from id' })
  @ApiResponse({ status: 200, description: 'Success' })
  update(@Param('id') id: number, @Body() updateRoleDto: UpdateRoleDto) {
    return this.roleService.update({ where: { id }, data: updateRoleDto });
  }

  @Delete(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'delete role from id' })
  @ApiResponse({ status: 200, description: 'Success' })
  remove(@Param('id') id: number) {
    return this.roleService.remove({ id });
  }
}
