import { Injectable } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { Prisma, Roles } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class RoleService {
  constructor(private prisma: PrismaService) { }

  async findOne(
    where: Prisma.RolesWhereUniqueInput,
  ): Promise<Roles | null> {
    return this.prisma.roles.findUnique({ where });
  }

  async findMany(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.RolesWhereUniqueInput;
    where?: Prisma.RolesWhereInput;
    orderBy?: Prisma.RolesOrderByWithRelationInput;
  }): Promise<Roles[]> {
    const { skip, take, cursor, where, orderBy } = params;
    return this.prisma.roles.findMany({ skip, take, cursor, where, orderBy });
  }

  async create(data: CreateRoleDto): Promise<Roles> {
    return this.prisma.roles.create({ data });
  }

  async update(params: {
    where: Prisma.RolesWhereUniqueInput;
    data: Prisma.RolesUpdateInput;
  }): Promise<Roles> {
    const { data, where } = params;
    return this.prisma.roles.update({ data, where });
  }

  async remove(where: Prisma.RolesWhereUniqueInput): Promise<Roles> {
    return this.prisma.roles.delete({ where });
  }
}
