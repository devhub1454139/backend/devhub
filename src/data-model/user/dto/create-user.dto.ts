import { ApiProperty } from "@nestjs/swagger";
import { Prisma } from "@prisma/client";
import { IsEmail, IsNotEmpty, Matches, MaxLength, MinLength, Validate } from "class-validator";

export class CreateUserDto implements Prisma.UserCreateInput {
    @ApiProperty({ description: "User login", required: true, example: "test" })
    @IsNotEmpty({ message: 'Это обязательное поле' })
    login: string;

    @ApiProperty({ description: "User email", required: true, example: "test@gmail.com" })
    @IsEmail({}, { message: 'Некорректный формат email-адреса' })
    @IsNotEmpty({ message: 'Это обязательное поле' })
    email: string;

    @ApiProperty({ description: "User password", required: true, example: "testtest" })
    @MinLength(5, { message: 'Пароль должен содержать не менее 5 символов' })
    @MaxLength(12, { message: 'Пароль должен содержать не более 12 символов' })
    password: string;

    @ApiProperty({ description: "User phone", required: false, example: "89990000000" })
    phone?: string;
    
    role?: Prisma.RolesCreateNestedOneWithoutUsersInput;

}