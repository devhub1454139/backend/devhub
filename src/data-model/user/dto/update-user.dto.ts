import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';
import { Prisma } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateUserDto extends PartialType(CreateUserDto)  implements Prisma.UserUpdateInput {
    @ApiProperty({ description: "User email", required: false, example: "test@gmail.com" })
    email?: string;
    @ApiProperty({ description: "User password", required: false, example: "testtest" })
    password?: string;
    @ApiProperty({ description: "User phone", required: false, example: "89990000000" })
    phone?: string;
    role?: Prisma.RolesCreateNestedOneWithoutUsersInput;
}
