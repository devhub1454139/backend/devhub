import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { Public } from 'src/security/auth/public/public.decorator';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('data-model')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Public()
  @Post()
  @ApiOperation({ summary: 'create a new user' })
  @ApiResponse({ status: 201, description: 'Successful create' })
  async signupUser(
    @Body()
    data: CreateUserDto,
  ): Promise<User> {
    data.role = {connect:{name:'user'}}
    return this.userService.create(data);
  }
  
  @Get()
  @ApiBearerAuth()
  @ApiOperation({ summary: 'get json of all users' })
  @ApiResponse({ status: 200, description: 'Success' })
  findAll() {
    return this.userService.findMany({});
  }

  @Get(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'get json of one user' })
  @ApiResponse({ status: 200, description: 'Success' })
  findOne(@Param('id') id: number) {
    return this.userService.findOne({ id });
  }

  @Patch(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'update user data' })
  @ApiResponse({ status: 200, description: 'Success' })
  update(@Param('id') id: number, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update({ where: { id }, data: updateUserDto });
  }

  @Delete(':id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'delete user' })
  @ApiResponse({ status: 200, description: 'Resource deleted successfully' })
  remove(@Param('id') id: number) {
    return this.userService.remove({ id });
  }
}
