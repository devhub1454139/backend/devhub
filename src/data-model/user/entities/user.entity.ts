import { User as UserModel } from "@prisma/client";
import { ApiProperty } from '@nestjs/swagger';

export class User implements UserModel {
    @ApiProperty({ example: 1, description: 'The user id' })
    id: number;
    @ApiProperty({ example: 'test', description: 'The user login (must be unique)' })
    login: string;
    @ApiProperty({ example: 'test@gmail.com', description: 'The user email (must be unique)' })
    email: string;
    @ApiProperty({ example: 'testtest', description: 'The user password (bcrypt hashing)' })
    password: string;
    @ApiProperty({ example: '89990000000', description: 'The user phone' })
    phone: string;
    @ApiProperty({ example: 'skip this', description: 'automatically put user' })
    roleid: number;
}
