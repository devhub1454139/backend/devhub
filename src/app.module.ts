import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { PrismaService } from './prisma/prisma.service';
import { RoleModule } from './data-model/role/role.module';
import { UserModule } from './data-model/user/user.module';
import { AuthModule } from './security/auth/auth.module';
import { UserService } from './data-model/user/user.service';
import { JwtAuthGuard } from './security/auth/guards/jwt-auth.guard';
import { APP_GUARD } from '@nestjs/core';

@Module({
  imports: [
    RoleModule, 
    UserModule, 
    AuthModule
  ],
  controllers: [AppController],
  providers: [
    PrismaService, 
    UserService,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    }],
})
export class AppModule {}
