import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { UserService } from 'src/data-model/user/user.service';

@Injectable()
export class AuthService {
  constructor(private userService: UserService, private jwtService: JwtService) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.userService.findOne({login: username});

    if (user && await bcrypt.compare(pass, user.password)) {
      const { password, ...result } = user;

      return result;
    }

    return null;
  }

  async login(user: any): Promise<any> {
    const { password, id, ...userData } = user,
        payload = { ...userData, sub: id };

    return { access_token: this.jwtService.sign(payload) };
  }
}
